<?php
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\HttpKernelInterface;
use Silex\Application;
date_default_timezone_set('America/Los_Angeles');

require_once __DIR__.'/../sso/sso.php';

$checkIn = function (Request $request, Application $app){
	$date = date('Y-n-j'); // Year-month-day
	$time = date('H:i');
	$netid = $request->headers->get('netId'); // need for DB lookups and insertions
	$user_type = $request->headers->get('UserType'); // needed for the proper redirect
	if($app['debug']) // debugging so should use SQLite
	{
		$stmt = $app['dbs']['sqlite']->prepare('SELECT shift, time_in, time_out FROM check_in_out WHERE date = :date AND netid = :netid AND time_out IS NULL'); // check to see if a record for this user for today exists that has not been checked out yet
		$stmt->bindParam(':date', $date);
		$stmt->bindParam(':netid', $netid);
		$stmt->execute();
		$stmt->setFetchMode(PDO::FETCH_ASSOC);
		if($stmt->rowCount() > 0) // there is at least one time entry for this user for today that has not been checked out yet
		{
			// should we error out, or force a checkout?
			return new Symfony\Component\HttpFoundation\Response('You cannot check in again until you have checked out of your current session', 401);
		}
		else // no record exists so we should create one
		{
			// shift number auto-increments, time_out does not need to be set
			$stmt = $app['dbs']['sqlite']->prepare('INSERT INTO check_in_out (`date`, netid, time_in, user_type) VALUES(:date, :netid, :time_in, :user_type)');
			$stmt->bindParam(':date', $date);
			$stmt->bindParam(':netid', $netid);
			$stmt->bindParam(':time_in', $time);
			$stmt->bindParam(':user_type', $user_type);
			$stmt->execute();
			// add stuff to the header for the redirect
			$request->headers->set('date', $date);
			$request->headers->set('timeIn', $time);
			$redirect = '/pages/default';
			// now setup the redirect
			switch($user_type){
				case 'Employee': $redirect = '/pages/employee';
					break;
				case 'Student': $redirect = '/pages/student';
					break;
				default: break;
			}
			// build the redirect request
			$subRequest = Request::create($redirect, 'GET', array(), $request->cookies->all(), array(), $request->server->all());
			if($request->getSession())
				$subRequest->setSession($request->getSession());
			return $redirect;
		}
	}
	else // otherwise, use the default
	{
		return 'NOT YET IMPLEMENTED';
	}
};

$app->match('/tracker/checkin', $checkIn)->before($ssoProtect);
?>
<?php
  $app->match('/this_is_a_test', function(){
	  return 'This is a test';
  });

  $app->match('/testing_db', function() use($app){
    $result =  $app['dbs']['sqlite']->query("SELECT * FROM student");

    if($result){
      return 'Database is working.';
    }
    else{
      return 'Database is not working.';
    }
  });

  $app->match('/pages/default', function(){
	return 'Checkin works.';
  });
?>
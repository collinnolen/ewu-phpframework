<?php
// Create the application
$app = new Silex\Application();

// Load the environment
$config_dir = __DIR__; // intent revealing
$config_file = '.env';
$environment = new Dotenv\Dotenv($config_dir); // more intent revealing to use explicit names
if (file_exists($config_dir.'/'.$config_file)) {
    $environment->load();
}
//$dotenv->required(['DB_HOST', 'DB_NAME', 'DB_USER', 'DB_PASS']);

// Set debug

if (strcasecmp(getenv('DEBUG'), 'true') == 0) {
    $app['debug'] = true;
}

// Register DBAL Service Provider
// If you are using SQLite, you need to set the 'path' variable to the database on the machine
// in the db.options 'sqlite' portion
$app->register(new Silex\Provider\DoctrineServiceProvider(), array(
    'dbs.options' => array(
		'default' => array(
			'driver' => getenv('FORMS_DB_DRIVER'),
			'host' => getenv('FORMS_DB_HOST'),
			'dbname' => getenv('FORMS_DB_DBNAME'),
			'user' => getenv('FORMS_DB_USER'),
			'password' => getenv('FORMS_DB_PASSWORD')
		),
		'sqlite' => array(
			'driver' => 'pdo_sqlite',
			'path' => __DIR__.'/../db/test.db'
		)
	)
));

// Register ORM Service Provider
$app->register(new Dflydev\Silex\Provider\DoctrineOrm\DoctrineOrmServiceProvider(), array(
    'orm.proxies_dir' => __DIR__.'/../db/proxies',
    'orm.em.options' => array(
        'mappings' => array(
            array(
                'type' => 'annotation',
                'namespace' => 'Forms\Test',
                'path' => __DIR__.'/../src/models',
//                'use_simple_annotation_reader' => false,
            ),
        ),
    ),
));

// Register a Twig Service Provider
$app->register(new Silex\Provider\TwigServiceProvider(), array(
    'twig.path' => __DIR__.'/views',
));

// Register a Translation Provider (needed for default form view)
$app->register(new Silex\Provider\TranslationServiceProvider(), array(
    'translator.messages' => array(),
));

// Register a Validation Service Provider
$app->register(new Silex\Provider\ValidatorServiceProvider());

// Register a Form Service Provider
$app->register(new Silex\Provider\FormServiceProvider());
?>


// Register gigablah
$app->register((new Gigablah\Silex\OAuth\OAuthServiceProvider(), array(
    'oauth.services' => array(
        'Facebook' => array(
            'key' => FACEBOOK_API_KEY,
            'secret' => FACEBOOK_API_SECRET,
            'scope' => array('email'),
            'user_endpoint' => 'https://graph.facebook.com/me'
        ),
        'Dropbox' => array(
            'key' => DROPBOX_API_KEY,
            'secret' => DROPBOX_API_SECRET,
            'scope' => array(),
            'user_endpoint' => 'https://api.dropbox.com/1/account/info'
        ),
        'Twitter' => array(
            'key' => TWITTER_API_KEY,
            'secret' => TWITTER_API_SECRET,
            'scope' => array(),
            'user_endpoint' => 'https://api.twitter.com/1.1/account/verify_credentials.json'
        ),
        'Google' => array(
            'key' => GOOGLE_API_KEY,
            'secret' => GOOGLE_API_SECRET,
            'scope' => array(
                'https://www.googleapis.com/auth/userinfo.email',
                'https://www.googleapis.com/auth/userinfo.profile'
            ),
            'user_endpoint' => 'https://www.googleapis.com/oauth2/v1/userinfo'
        ),
        'GitHub' => array(
            'key' => GITHUB_API_KEY,
            'secret' => GITHUB_API_SECRET,
            'scope' => array('user:email'),
            'user_endpoint' => 'https://api.github.com/user'
        ),
	'BitBucket' => array(
            'key' => 'brwkawCAHNLZEuxTGg',
            'secret' => 'H2VNj5HapjUmLEb6qRwbgwGk2U8MQ4RF',
            'scope' => array('repository'),
            'user_endpoint' => 'https://bitbucket.org/!api/1.0/oauth/request_token'
        ),
    )
));

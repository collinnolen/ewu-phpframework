<?php
use Symfony\Component\HttpFoundation\Request;

include __DIR__.'/../models/test.php';
include __DIR__.'/../forms/TestForm.php';

$test = $app['controllers_factory'];
$test->get('/', function () {
    return 'Test Home Page';
});

$test->match('/view', function () use ($app) {
    return $app['twig']->render('hello.twig');
});

$test->match('/viewX', function () use ($app) {
    return $app['twig']->render('helloX.twig', array('test'=>'test'));
});

$test->match('/form', function (Request $request) use ($app) {

    $entry = new Forms\Test\Entry();
    $user = null;
    $em = $app['orm.em'];
    $netId = $request->headers->get('netId');

    // Create the Form
    $form = $app['form.factory']->create('Forms\Test\TestForm', $entry);

    // Populate Request information into the form
    $form->handleRequest($request);

    // Handle Responses for submitted forms
    if ($form->isValid()) {

        // It looks like the underlying object is populated by handleRequest
        // $data = $form->getData();

        $user=$em->find('Forms\Test\User', $netId) ?
                                                   : $user = new Forms\Test\User($netId);
        $entry->setUser($user);
        $em->persist($user);
        $em->persist($entry);
        $em->flush();

        return "Thank you!";
    }

    // Display the form
    return $app['twig']->render('test.twig', array('form' => $form->createView(), "netId"=>$netId));

})
->before($ssoProtect)
;


return $test;
